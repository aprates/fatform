# FatScript Console Form Component Library

## Introduction

`fatForm` is a form component library tailored for FatScript. It helps developers to seamlessly use console-based UI components such as text inputs, buttons, checkboxes, dividers, and more.

## Features

- Extensible component-based architecture
- Built-in event handling with event buses
- Managed focus switching between components
- Customizable color theming for active and blur states

## Installation

To leverage `fatForm`, you must have `fry` (^3.4.0), the FatScript interpreter, installed. Follow these steps to get started:

1. **Install `fry` interpreter**:

- Detailed installation instructions are available on the [official FatScript website](https://fatscript.org).

2. **Navigate to your main project directory**:

```bash
cd <my-project-folder>
```

3. **Clone the `fatForm` repository as a submodule**:

```bash
git submodule add https://gitlab.com/aprates/fatform.git fatForm
```

4. **Import `fatForm` as needed**:

```
fatForm <- fatForm.lib
```

## Basic Usage

### Setting Up

Start by importing the `fatForm` library and other essential modules:

```
fatForm <- fatForm.lib
system  <- fat.system
curses  <- fat.curses
color   <- fat.color
```

### Creating a Theme

For aesthetic customization of your form components, set up a theme:

```
theme = {
  borderColor = curses.makePair(color.blue)
  labelColor = curses.makePair(color.yellow)
  activeColor = curses.makePair(color.red)
}
```

### Building a Form

Employ the components provided by the library to craft a form layout:

```
{ Box, Button, Checkbox, Divider, Event, Label, TextInput } = fatForm

myForm = Box(
  # ... your components and configurations go here ...
)
```

### Event Handling

To capture and handle from events, establish listeners:

```fat
handleChange = (event: Event) -> {
  # ... logic for handling the event ...
}

fatForm.onChange.addListener(handleChange)
```

### Rendering the Form

Lastly, invoke the `show` method to display the form:

```
fatForm.show(myForm)
```

## Sample Forms

While there isn't a comprehensive documentation detailing the properties of each component, their design is consistent and intuitive. You can refer to the provided samples to get a feel for their usage or delve directly into the implementation of each component for deeper insight.

### Sample 1: TextInput and Checkbox

This demo showcases the implementation of text inputs, dividers, and checkboxes. Typing "exit" into the TextInput will terminate the demonstration.

View [`sample1.fat`](sample1.fat) for the entire code.

Or try running it like:

```
fry sample1.fat
```

### Sample 2: Buttons with Navigation

This sample lets users switch between buttons using the up and down arrow keys. Hitting the "Exit Game" button will also shut down the application.

View [`sample2.fat`](sample2.fat) for the detailed code.

Or try running it like:

```
fry sample2.fat
```

## Contributing

For feature suggestions or enhancement proposals, kindly initiate an issue on [fatForm's GitLab](https://gitlab.com/aprates/fatform/issues).

### Donations

Should you find `fatForm` valuable and wish to back its progress, please consider [buying me a coffee](https://www.buymeacoffee.com/aprates).

## License

[MIT](LICENSE) © 2023-2024 Antonio Prates.
