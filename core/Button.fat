# FatScript Console Form Component Library
# fatForm project <https://gitlab.com/aprates/fatform>
# Original author: Antonio Prates
# License: MIT

Button = (

  Component

  ~ id: Text = null
  ~ pos: Scope
  ~ label: Text
  ~ width: Number = 30
  ~ blurColor: Number      # color pair
  ~ activeColor: Number    # color pair
  ~ disabledColor: Number  # color pair
  ~ isActive: Boolean = false
  ~ isDisabled: Boolean = false

  apply = <> self.init(self)

  render = -> {
    { id, pos, label, width, blurColor, activeColor, disabledColor } = self

    # set up where to print
    base = _ ?? Position(0, 0)
    origin = Position(base.x + pos.x, base.y + pos.y)
    corner = Position(origin.x + width, origin.y + 2)

    # button print method
    print = <> {
      noArrow = '   '
      arrow = self.isActive ? ' <-' : noArrow
      label = (noArrow + label + arrow).overlay(' '.repeat(width - 1), 'center')
      curses.box(origin, corner)
      curses.printAt(Position(origin.x + 1, origin.y + 1), label)
    }

    self.isDisabled ? {
      disabledColor ? curses.usePair(disabledColor)
    } : self.isActive ? {
      activeColor ? curses.usePair(activeColor)
      print

      ~ key = curses.readKey

      key ? fatForm.onChange.produce(id, key)
    } : {
      blurColor ? curses.usePair(blurColor)
    }

    print
  }

)
